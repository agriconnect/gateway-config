#!/bin/bash

# Script to enable PWM ports and set owner to debian, so that we can use
# it under user "debian" (no need to be root)
# Ref: https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-pwm
# The Device Tree overlay BB-PWM1-00A0.dts
# must be loaded previously.

BASEDIR='/sys/class/pwm/pwmchip0/'

if [ -d "$BASEDIR" ]; then
    echo "Enable PWM ports in pwmchip0"
    echo "Expose pwm0"
    echo 0 > ${BASEDIR}export
    echo "Expose pwm1"
    echo 1 > ${BASEDIR}export
    chown debian:debian -R ${BASEDIR}pwm0/
    chown debian:debian -R ${BASEDIR}pwm1/
else
    echo "$BASEDIR does not exist. Please add BB-PWM1 overlay to Cape Manager."
fi
