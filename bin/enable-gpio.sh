#!/bin/bash

# Script to enable GPIO port and set owner to debian, so that we can use
# it under user "debian" (no need to be root)
# Ref: https://www.kernel.org/doc/Documentation/gpio/sysfs.txt

re_number='^[0-9]+$'
BASEDIR='/sys/class/gpio/'

function help {
    echo "Usage: $0 port_number [-o [-h]]"
    echo "where port_number is 1, 2, 3..."
    echo "Option:"
    echo "   -o: Make this port output and active low, set high as initial"
    echo "   -h: Make this port active high, and set low as initial"
}

if ! [[ $1 =~ $re_number ]] ; then
   echo "error: $1 is not a number" >&2
   help
   exit 1
fi

if [[ $1 == '0' ]] ; then
    echo "error: Need to be a positive number" >&2
    help
    exit
fi

echo "Enable GPIO port $1"
DEVDIR=${BASEDIR}gpio$1/
if [ ! -d "$DEVDIR" ]; then
    echo "$1" > ${BASEDIR}export
fi
if [[ $2 == '-o' ]] ; then
    echo "Make GPIO $1 output"
    # Write "high" will set the initial value for output
    # as 1, and prevent peripheral from being turned on
    if [[ $3 == '-h' ]] ; then
        echo 1 > ${DEVDIR}/active_low
        echo "low" > ${DEVDIR}direction
    else
        echo "high" > ${DEVDIR}direction
    fi
fi
echo "Set owner 'debian' for $DEVDIR"
chown debian:debian -R $DEVDIR
